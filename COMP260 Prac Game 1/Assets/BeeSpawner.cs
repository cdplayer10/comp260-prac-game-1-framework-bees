﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour {
    public int nBees = 50;
    // Use this for initialization
    public BeeMove beePrefab;
    public PlayerMove player;
    public PlayerMove player2;
    public Rect spawnRect;
	public float beePeriod = 10;
	public int periodMax=15;
	public int periodMin=3;

    void OnDrawGizmos()
    {
        // draw the spawning rectangle
        Gizmos.color = Color.green;
        Gizmos.DrawLine(
                new Vector2(spawnRect.xMin, spawnRect.yMin),
                new Vector2(spawnRect.xMax, spawnRect.yMin));
        Gizmos.DrawLine(
                new Vector2(spawnRect.xMax, spawnRect.yMin),
                new Vector2(spawnRect.xMax, spawnRect.yMax));
        Gizmos.DrawLine(
                new Vector2(spawnRect.xMax, spawnRect.yMax),
                new Vector2(spawnRect.xMin, spawnRect.yMax));
        Gizmos.DrawLine(
                new Vector2(spawnRect.xMin, spawnRect.yMax),
                new Vector2(spawnRect.xMin, spawnRect.yMin));
    }

    void Start()
    {
        // instantiate a bee
        for (int i = 0; i < nBees; i++)
        {
			
            BeeMove bee = Instantiate(beePrefab);
            // attach to this object in the hierarchy
            bee.transform.parent = transform;
            // give the bee a name and number
            bee.gameObject.name = "Bee " + i;
            bee.target = player.transform;
            bee.target2 = player2.transform;
            float x = spawnRect.xMin +
                Random.value * spawnRect.width;
            float y = spawnRect.yMin +
                Random.value * spawnRect.height;
            bee.transform.position = new Vector2(x, y);


        }

    }
    public void DestroyBees(Vector2 centre, float radius)
    {
        // destroy all bees within ‘radius’ of ‘centre’
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            // BUG! the line below doesn’t work
            Vector2 v = (Vector2)child.position - centre;
            if (v.magnitude <= radius)
            {
                Destroy(child.gameObject);
            }
        }
    }



    // Update is called once per frame
    void Update () {
		
		beePeriod = beePeriod - Time.deltaTime;
		if (beePeriod < 0){
			float i=0;
			BeeMove bee = Instantiate(beePrefab);
			// attach to this object in the hierarchy
			bee.transform.parent = transform;
			// give the bee a name and number
			bee.gameObject.name = "Bee " + nBees+i;
			bee.target = player.transform;
			bee.target2 = player2.transform;
			float x = spawnRect.xMin +
				Random.value * spawnRect.width;
			float y = spawnRect.yMin +
				Random.value * spawnRect.height;
			bee.transform.position = new Vector2(x, y);
			i=i+1;
			beePeriod = (Random.Range(periodMin, periodMax));
	}
}
}
