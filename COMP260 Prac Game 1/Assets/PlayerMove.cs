﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {


    public string axisX = "Horizontal";
    public string axisY = "Vertical";
    public float maxSpeed = 5.0f; // in metres per second
    public float acceleration = 1.0f; // in metres/second/second
    private float speed = 0.0f;    // in metres/second
    public float brake = 5.0f; // in metres/second/second
    public float turnSpeed = 30.0f; // in degrees/second
    private BeeSpawner beeSpawner;
    public float destroyRadius = 1.0f;

    void Start()
    {
        // find the bee spawner and store a reference for later
        beeSpawner = FindObjectOfType<BeeSpawner>();
    }


    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            // destroy nearby bees
            beeSpawner.DestroyBees(
                transform.position, destroyRadius);
        }

        // the horizontal axis controls the turn
        float turn = Input.GetAxis(axisX);
        float CarSpeed;
        if (Mathf.Round(speed) == 0){
            CarSpeed = 0;
        }
        else
        {
            CarSpeed = 5 / Mathf.Round(speed);
        }
        

        // turn the car
        transform.Rotate(0, 0, -turn * turnSpeed * CarSpeed * Time.deltaTime);


        // the vertical axis controls acceleration fwd/back
        float forwards = Input.GetAxis(axisY);
        if (forwards > 0)
        {
            // accelerate forwards
            speed = speed + acceleration * Time.deltaTime;
        }
        else if (forwards < 0)
        {
            // accelerate backwards
            speed = speed - acceleration * Time.deltaTime;
        }
        else
        {
            // braking
            if (speed > 0)
            {
                speed = speed - brake * Time.deltaTime;
                if (speed < 0)
                {
                    speed = 0;
                }
            }
            else
            {
                speed = speed + brake * Time.deltaTime;
                if (speed > 0)
                {
                    speed = 0;
                }
            }
        }

        speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);

        // compute a vector in the up direction of length speed
        Vector2 velocity = Vector2.up * speed;

        // move the object
        transform.Translate(velocity * Time.deltaTime, Space.Self);
    }


    


	//void Update() {
		// get the input values
		Vector2 direction;
	//	direction.x = Input.GetAxis(axisX);
	//	direction.y = Input.GetAxis(axisY);

		// scale by the maxSpeed parameter
	//	Vector2 velocity = direction * maxSpeed;

		// move the object
	//	transform.Translate(velocity * Time.deltaTime);
	//}



}
