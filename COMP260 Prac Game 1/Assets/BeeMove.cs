﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {

    // Use this for initialization

    // Update is called once per frame
    public PlayerMove player;
    public PlayerMove player2;
    public float minSpeed = 0;
    public float maxSpeed = 10;
    public float minTurnSpeed = 1;
    public float maxTurnSpeed = 5;
    private float speed = 4.0f;        // metres per second
	private float turnSpeed = 180.0f;  // degrees per second
	public Transform target;
	public Transform target2;
	public Vector2 direction;
	private Vector2 heading = Vector2.right;
	public ParticleSystem explosionPrefab;
    void Start()
    {
        // find the player to set the target
        target = player.transform;
        target2 = player2.transform;

        // bee initially moves in random direction
        heading = Vector2.right;
        float angle = Random.value * 360;
        heading = heading.Rotate(angle);

        // set speed and turnSpeed randomly 
        speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
        turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed,Random.value);
    }




    void Update() {
		// get the vector from the bee to the target \
		if ((target.position - transform.position).magnitude > (target2.position - transform.position).magnitude) {
			direction = target2.position - transform.position;
		} else {
			direction = target.position - transform.position;
		}
		// calculate how much to turn per frame
		float angle = turnSpeed * Time.deltaTime;

		// turn left or right
		if (direction.IsOnLeft(heading)) {
			// target on left, rotate anticlockwise
			heading = heading.Rotate(angle);
		}
		else {
			// target on right, rotate clockwise
			heading = heading.Rotate(-angle);
		}

		transform.Translate(heading * speed * Time.deltaTime);
	}
	void OnDrawGizmos() {
		// draw heading vector in red
		Gizmos.color = Color.red;
		Gizmos.DrawRay(transform.position, heading);

		// draw target vector in yellow
		Gizmos.color = Color.yellow;
		Vector2 direction = target.position - transform.position;
		Gizmos.DrawRay(transform.position, direction);
	}
	void OnDestroy() {
		// create an explosion at the bee's current position
		ParticleSystem explosion = Instantiate(explosionPrefab);
		explosion.transform.position = transform.position;
		// destroy the particle system when it is over
		Destroy(explosion.gameObject, explosion.duration);
	}





}
